﻿using System.Diagnostics;
using System.Globalization;
using SalesReporterKata;

public static class Program
{

	public static void log(string message)
	{
		Console.WriteLine(message);
	}

	public static void printArg(string[] dataContentString)
	{
		//get the header line  
		string headerLine = dataContentString[0];  
		//get other content lines  
		var valueLine = dataContentString.Skip(1);
		var columnInfos = new List<(int index, int size, string name)>();
		//build the header of the table with column names from our data file  
		int i = 0;
		foreach (var columName in headerLine.Split(','))
		{
			columnInfos.Add((i++, columName.Length, columName));
		}

		var headerString  = String.Join(
			" | ", 
			columnInfos.Select(x=>x.name).Select(
				(val,ind) => val.PadLeft(16)));
		log("+" + new String('-', headerString.Length + 2) + "+");
		log("| " + headerString + " |");
		log("+" + new String('-', headerString.Length +2 ) + "+");

		//then add each line to the table  
		foreach (string line in valueLine)  
		{ 
			//extract columns from our csv line and add all these cells to the line  
			var cells = line.Split(',');
			var tableLine  = String.Join(
				" | ", 
		            
				line.Split(',').Select(
					(val,ind) => val.PadLeft(16)));
			log($"| {tableLine} |");
		} 
		log("+" + new String('-', headerString.Length+2) + "+");

		// if command is report
	}

	public static void reportArg(string[] dataContentString)
	{
		//get all the lines without the header in the first line  
		var otherLines = dataContentString.Skip(1);  
		//declare variables for our conters  
		int number1 = 0, number2 = 0;  
		double number4 = 0.0, number5 = 0.0, number3 = 0;  
		HashSet<string> clients = new HashSet<string>();  
		DateTime last = DateTime.MinValue;  
		//do the counts for each line  
		foreach (var line in otherLines)  
		{ //get the cell values for the line  
			var cells = line.Split(',');  
			number1++;//increment the total of sales  
			//to count the number of clients, we put only distinct names in a hashset //then we'll count the number of entries if (!clients.Contains(cells[1])) clients.Add(cells[1]);  
			number2 += int.Parse(cells[2]);//we sum the total of items sold here
			number3 += double.Parse(cells[3], CultureInfo.InvariantCulture);//we sum the amount of each sell
			//we compare the current cell date with the stored one and pick the higher last = DateTime.Parse(cells[4]) > last ? DateTime.Parse(cells[4]) : last;  
		} 
		//we compute the average basket amount per sale  
		number4 = Math.Round(number3 / number1,2);  
		//we compute the average item price sold  
		number5 = Math.Round(number3 / number2,2);  
		log($"+{new String('-',45)}+");
		log($"| {" Number of sales".PadLeft(30)} | {number1.ToString().PadLeft(10)} |");
		log($"| {" Number of clients".PadLeft(30)} | {clients.Count.ToString().PadLeft(10)} |");
		log($"| {" Total items sold".PadLeft(30)} | {number2.ToString().PadLeft(10)} |");
		log($"| {" Total sales amount".PadLeft(30)} | {Math.Round(number3,2).ToString().PadLeft(10)} |");
		log($"| {" Average amount/sale".PadLeft(30)} | {number4.ToString().PadLeft(10)} |");
		log($"| {" Average item price".PadLeft(30)} | {number5.ToString().PadLeft(10)} |");
		log($"+{new String('-',45)}+");
	}

	public static void welcomeTitle()
	{
		//add a title to our app 
		log("=== Sales Viewer ===");
	}

	public static AppOption extractCommand()
	{
		return new AppOption(File.ReadAllLines("../../../data.csv"),"unknown");
	}
	
	public static void Main(string[] args)
	{
		welcomeTitle();
		AppOption option = extractCommand();
 
		//if command is print  
		if (option.Command == "print")  
		{  
			printArg(option.DataContent);
		} 
		else if (option.Command == "report")  
		{  
			reportArg(option.DataContent);
		}  
		else  
		{  
 			 log("[ERR] your command is not valid ");  
			 log("Help: ");  
			 log("    - [print]  : show the content of our commerce records in data.csv");  
			 log("    - [report] : show a summary from data.csv records ");  
		}
	}
}
