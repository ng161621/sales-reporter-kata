﻿namespace SalesReporterKata;

public class AppOption
{
    private string[] dataContent;
    private string command;

    public AppOption(string[] dataContent, string command)
    {
        this.dataContent = dataContent;
        this.command = command;
    }

    public string[] DataContent
    {
        get => dataContent;
        set => dataContent = value ?? throw new ArgumentNullException(nameof(value));
    }

    public string Command
    {
        get => command;
        set => command = value ?? throw new ArgumentNullException(nameof(value));
    }
}